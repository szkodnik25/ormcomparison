package pl.wrzos.ormcomparison.activity.main;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import com.github.mikephil.charting.charts.BarChart;
import pl.wrzos.ormcomparison.R;
import pl.wrzos.ormcomparison.activity.ActivityPresenter;
import pl.wrzos.ormcomparison.activity.benchmark.BenchmarkActivity;
import pl.wrzos.ormcomparison.chart.core.BarChartBuilder;
import pl.wrzos.ormcomparison.chart.core.BenchmarkChartResult;
import pl.wrzos.ormcomparison.chart.core.ChartBuilder;
import pl.wrzos.ormcomparison.chart.core.SingleOperationResult;
import pl.wrzos.ormcomparison.utils.DateUtils;
import pl.wrzos.ormcomparison.utils.Preferences;

public class MainActivityPresenter extends ActivityPresenter<MainActivityView> {

	private BarChartBuilder barChartBuilder = new BarChartBuilder();

	public MainActivityPresenter(MainActivityView view) {
		super(view);
	}

	@Override
	public void onResume() {
		super.onResume();
		BenchmarkChartResult lastResult = Preferences.getBenchmarkResult(getView().getContext());
		if (lastResult != null) {
			showResult(lastResult);
		}
	}

	ChartBuilder<BarChart, SingleOperationResult> getChartBuilder() {
		return barChartBuilder;
	}

	void runBenchmark() {
		BenchmarkChartResult lastResult = Preferences.getBenchmarkResult(getView().getContext());
		if (lastResult != null) {
			new AlertDialog.Builder(getView().getContext())
				.setTitle(R.string.benchmark_detected_title)
				.setMessage(getView().getContext()
									 .getString(
										 R.string.benchmark_detected_msg,
										 DateUtils.formatDate(lastResult.getBenchmarkDate())))
				.setPositiveButton(R.string.yes, (dialogInterface, i) -> {
					Preferences.saveBenchmarkResult(getView().getContext(), null);
					runBenchmark();
				})
				.setNegativeButton(R.string.cancel, null).show();
		} else {
			getView().getContext().startActivity(new Intent(getView().getContext(), BenchmarkActivity.class));
		}
	}

	private void showResult(BenchmarkChartResult result) {
		getView().showCreateBenchmarkResult(result.getCreateResult());
		getView().showWriteBenchmarkResult(result.getWriteResult());
		getView().showReadBenchmarkResult(result.getReadResult());
		getView().showUpdateBenchmarkResult(result.getUpdateResult());
		getView().showDeleteBenchmarkResult(result.getUpdateResult());
	}
}
