package pl.wrzos.ormcomparison.activity.main;

import pl.wrzos.ormcomparison.activity.ActivityView;
import pl.wrzos.ormcomparison.chart.core.SingleOperationResult;

public interface MainActivityView extends ActivityView {
	void showCreateBenchmarkResult(SingleOperationResult createResult);

	void showWriteBenchmarkResult(SingleOperationResult writeResult);

	void showReadBenchmarkResult(SingleOperationResult readResult);

	void showUpdateBenchmarkResult(SingleOperationResult updateResult);

	void showDeleteBenchmarkResult(SingleOperationResult deleteResult);
}
