package pl.wrzos.ormcomparison.activity.benchmark;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import pl.wrzos.ormcomparison.R;
import pl.wrzos.ormcomparison.activity.ActivityPresenter;
import pl.wrzos.ormcomparison.benchmark.core.BenchmarkConstants;
import pl.wrzos.ormcomparison.benchmark.core.BenchmarkResult;
import pl.wrzos.ormcomparison.chart.core.BenchmarkChartResult;
import pl.wrzos.ormcomparison.greendao.GreenDaoBenchmark;
import pl.wrzos.ormcomparison.ormlite.OrmLiteBenchmark;
import pl.wrzos.ormcomparison.sugarorm.SugarOrmBenchmark;
import pl.wrzos.ormcomparison.utils.Preferences;

public class BenchmarkActivityPresenter extends ActivityPresenter<BenchmarkActivityView> {

	public static final String TAG = BenchmarkActivityPresenter.class.getSimpleName();
	public static final String STATE_RESULT = "result";

	private List<BenchmarkResult> benchmarkResults;
	private boolean benchmarkRunning = false;

	public BenchmarkActivityPresenter(BenchmarkActivityView view) {
		super(view);
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getView().setBenchmarkDescription(getView().getContext().getString(R.string.orm_benchmark_desc,
																		   BenchmarkConstants.STRING_LENGTH,
																		   BenchmarkConstants.BYTE_ARRAY_SIZE,
																		   BenchmarkConstants.NUM_OF_INSERTS));
		if (savedInstanceState != null) {
			Parcelable[] benchmarkResultState = savedInstanceState.getParcelableArray(STATE_RESULT);
			if (benchmarkResultState != null && benchmarkResultState.length > 0) {
				consumeResult(Arrays.stream(benchmarkResultState)
									.map(parcelable -> (BenchmarkResult) parcelable)
									.collect(Collectors.toList()));
			}
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		if (benchmarkResults != null && !benchmarkResults.isEmpty()) {
			outState.putParcelableArray(
				STATE_RESULT, benchmarkResults.toArray(new Parcelable[benchmarkResults.size()]));
		}
	}

	void runBenchmark() {
		benchmarkRunning = true;
		getView().setBenchmarkButtonVisibility(false);
		getView().setProgressVisibility(true);

		Observable<BenchmarkResult> greenDaoBenchmarkObservable = Observable.fromCallable(this::callGreenDaoBenchmark);
		Observable<BenchmarkResult> ormLiteBenchmarkObservable  = Observable.fromCallable(this::callOrmLiteBenchmark);
		Observable<BenchmarkResult> sugarOrmBenchmarkObservable = Observable.fromCallable(this::callSugarOrmBenchmark);

		Observable.zip(
			greenDaoBenchmarkObservable, ormLiteBenchmarkObservable, sugarOrmBenchmarkObservable,
			(greenDao, ormLite, sugarOrm) -> Arrays.asList(greenDao, ormLite, sugarOrm))
				  .subscribeOn(Schedulers.io())
				  .observeOn(AndroidSchedulers.mainThread())
				  .subscribe(this::consumeResult);
	}

	BenchmarkResult callGreenDaoBenchmark() {
		return new GreenDaoBenchmark(getView().getContext()).performSync();
	}

	BenchmarkResult callOrmLiteBenchmark() {
		return new OrmLiteBenchmark(getView().getContext()).performSync();
	}

	BenchmarkResult callSugarOrmBenchmark() {
		return new SugarOrmBenchmark().performSync();
	}

	void saveResult(BenchmarkChartResult result) {
		result.setBenchmarkDate(new Date());
		Preferences.saveBenchmarkResult(getView().getContext(), result);
	}

	void consumeResult(List<BenchmarkResult> results) {
		benchmarkRunning = false;
		if (results == null || results.isEmpty()) {
			return;
		}
		this.benchmarkResults = results;
		getView().setBenchmarkButtonVisibility(false);
		getView().setProgressVisibility(false);
		saveResult(BenchmarkChartResult.wrap(results));
		double summary = 0.0;
		for (BenchmarkResult result : results) {
			summary += result.getSummaryInSec();
			String currentSummary = String.format(Locale.getDefault(), "%.2fs", result.getSummaryInSec());
			switch (result.getOrm()) {
				case GREEN_DAO:
					getView().setGreenDaoResultText(currentSummary);
					break;
				case ORM_LITE:
					getView().setOrmLiteResultText(currentSummary);
					break;
				case SUGAR_ORM:
					getView().setSugarOrmResultText(currentSummary);
					break;
				default:
					break;
			}
		}
		getView().setSummaryTextVisibility(true);
		getView().setSummaryText(getView().getContext().getString(R.string.benchmark_summary_result, summary));
		getView().showResultsComponents();
	}

	boolean benchmarkNotRunning() {
		if (benchmarkRunning) {
			Snackbar snackbar = Snackbar.make(getView().getRootView(), R.string.be_patient, Snackbar.LENGTH_SHORT);
			snackbar.getView()
					.setBackgroundColor(ContextCompat.getColor(getView().getContext(), R.color.snack_bar_error));
			snackbar.show();
		}
		return !benchmarkRunning;
	}
}
