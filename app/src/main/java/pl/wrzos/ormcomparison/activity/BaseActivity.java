package pl.wrzos.ormcomparison.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import butterknife.ButterKnife;
import pl.wrzos.ormcomparison.R;

public abstract class BaseActivity<Presenter extends ActivityPresenter>
	extends AppCompatActivity implements ActivityView {

	public static final String TAG = BaseActivity.class.getSimpleName();

	private Presenter presenter;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		presenter = providePresenter();
		if (getLayoutRes() == R.layout.activity_base) {
			Log.e(
				TAG,
				"You did not override getLayoutRes() method (which you should do), so default layout will be used.");
		}
		setContentView(getLayoutRes());
		ButterKnife.bind(this);
		getPresenter().onCreate(savedInstanceState);
	}

	@Override
	protected void onResume() {
		super.onResume();
		getPresenter().onResume();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		getPresenter().onSaveInstanceState(outState);
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onDestroy() {
		getPresenter().onDestroy();
		super.onDestroy();
	}

	protected abstract Presenter providePresenter();

	public Presenter getPresenter() {
		return presenter;
	}

	@Override
	public Context getContext() {
		return this;
	}
}
