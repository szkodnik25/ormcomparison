package pl.wrzos.ormcomparison.activity.benchmark;

import android.view.View;
import pl.wrzos.ormcomparison.activity.ActivityView;

public interface BenchmarkActivityView extends ActivityView {
	View getRootView();

	void setBenchmarkDescription(String description);

	void setBenchmarkButtonVisibility(boolean visible);

	void setProgressVisibility(boolean visible);

	void setSummaryTextVisibility(boolean visible);

	void setSummaryText(String text);

	void setGreenDaoResultText(String text);

	void setOrmLiteResultText(String text);

	void setSugarOrmResultText(String text);

	void showResultsComponents();
}
