package pl.wrzos.ormcomparison.activity;

import android.content.Context;
import pl.wrzos.ormcomparison.R;

public interface ActivityView {
	Context getContext();

	default int getLayoutRes() {
		return R.layout.activity_base;
	}
}
