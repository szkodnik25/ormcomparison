package pl.wrzos.ormcomparison.activity.main;

import butterknife.BindView;
import butterknife.OnClick;
import com.github.mikephil.charting.charts.BarChart;
import pl.wrzos.ormcomparison.R;
import pl.wrzos.ormcomparison.activity.BaseActivity;
import pl.wrzos.ormcomparison.chart.core.SingleOperationResult;

public class MainActivity extends BaseActivity<MainActivityPresenter> implements MainActivityView {

	public static final String TAG = MainActivity.class.getSimpleName();

	@BindView(R.id.create_bar_chart) BarChart createBarChart;
	@BindView(R.id.write_bar_chart)  BarChart writeBarChart;
	@BindView(R.id.read_bar_chart)   BarChart readBarChart;
	@BindView(R.id.update_bar_chart) BarChart updateBarChart;
	@BindView(R.id.delete_bar_chart) BarChart deleteBarChart;

	@Override
	public int getLayoutRes() {
		return R.layout.activity_main;
	}

	@Override
	protected MainActivityPresenter providePresenter() {
		return new MainActivityPresenter(this);
	}

	@OnClick(R.id.open_benchmark_screen)
	public void onRunBenchmarkClicked() {
		getPresenter().runBenchmark();
	}

	@Override
	public void showCreateBenchmarkResult(SingleOperationResult createResult) {
		getPresenter().getChartBuilder().on(createBarChart).show(createResult);
	}

	@Override
	public void showWriteBenchmarkResult(SingleOperationResult writeResult) {
		getPresenter().getChartBuilder().on(writeBarChart).show(writeResult);
	}

	@Override
	public void showReadBenchmarkResult(SingleOperationResult readResult) {
		getPresenter().getChartBuilder().on(readBarChart).show(readResult);
	}

	@Override
	public void showUpdateBenchmarkResult(SingleOperationResult updateResult) {
		getPresenter().getChartBuilder().on(updateBarChart).show(updateResult);
	}

	@Override
	public void showDeleteBenchmarkResult(SingleOperationResult deleteResult) {
		getPresenter().getChartBuilder().on(deleteBarChart).show(deleteResult);
	}
}
