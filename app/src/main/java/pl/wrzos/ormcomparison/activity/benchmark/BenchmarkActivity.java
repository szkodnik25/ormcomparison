package pl.wrzos.ormcomparison.activity.benchmark;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import java.util.Arrays;
import pl.wrzos.ormcomparison.R;
import pl.wrzos.ormcomparison.activity.BaseActivity;
import pl.wrzos.ormcomparison.utils.Animations;

public class BenchmarkActivity extends BaseActivity<BenchmarkActivityPresenter> implements BenchmarkActivityView {

	@BindView(R.id.root_view)      View     rootView;
	@BindView(R.id.benchmark_desc) TextView benchmarkDescription;

	@BindView(R.id.green_dao_view) View greenDaoView;
	@BindView(R.id.orm_lite_view)  View ormLiteView;
	@BindView(R.id.sugar_orm_view) View sugarOrmView;

	@BindView(R.id.benchmark_progress) View   benchmarkProgress;
	@BindView(R.id.run_benchmark)      Button benchmarkButton;
	@BindView(R.id.benchmark_done)     Button benchmarkDone;

	@BindView(R.id.benchmark_summary_text) TextView summaryText;
	@BindView(R.id.green_dao_result_text)  TextView greenDaoResultTextView;
	@BindView(R.id.orm_lite_result_text)   TextView ormLiteResultTextView;
	@BindView(R.id.sugar_orm_result_text)  TextView sugarOrmResultTextView;

	@Override
	public int getLayoutRes() {
		return R.layout.activity_benchmark;
	}

	@Override
	public View getRootView() {
		return rootView;
	}

	@Override
	public void setBenchmarkDescription(String description) {
		benchmarkDescription.setText(description);
	}

	@Override
	protected BenchmarkActivityPresenter providePresenter() {
		return new BenchmarkActivityPresenter(this);
	}

	@Override
	public void setBenchmarkButtonVisibility(boolean visible) {
		if (visible) {
			Animations.showView(benchmarkButton);
		} else {
			Animations.hideView(benchmarkButton);
		}
	}

	@Override
	public void setProgressVisibility(boolean visible) {
		if (visible) {
			Animations.showView(benchmarkProgress);
		} else {
			Animations.hideView(benchmarkProgress);
		}
	}

	@Override
	public void setSummaryTextVisibility(boolean visible) {
		if (visible) {
			Animations.showView(summaryText);
		} else {
			Animations.hideView(summaryText);
		}
	}

	@Override
	public void setSummaryText(String text) {
		summaryText.setText(text);
	}

	@Override
	public void setGreenDaoResultText(String text) {
		greenDaoResultTextView.setText(text);
	}

	@Override
	public void setOrmLiteResultText(String text) {
		ormLiteResultTextView.setText(text);
	}

	@Override
	public void setSugarOrmResultText(String text) {
		sugarOrmResultTextView.setText(text);
	}

	@Override
	public void showResultsComponents() {
		Animations.showAll(Arrays.asList(greenDaoView, ormLiteView, sugarOrmView, benchmarkDone), 100);
	}

	@Override
	public void onBackPressed() {
		if (getPresenter().benchmarkNotRunning()) {
			super.onBackPressed();
		}
	}

	@OnClick(R.id.run_benchmark)
	public void onRunBenchmarkClicked() {
		getPresenter().runBenchmark();
	}

	@OnClick(R.id.benchmark_done)
	public void onDoneClicked() {
		onBackPressed();
	}
}
