package pl.wrzos.ormcomparison.activity;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;

public abstract class ActivityPresenter<View extends ActivityView> {

	protected View view;

	public ActivityPresenter(View view) {
		this.view = view;
	}

	@CallSuper
	public void onCreate(@Nullable Bundle savedInstanceState) {

	}

	public void onSaveInstanceState(Bundle outState) {

	}

	@CallSuper
	public void onResume() {

	}

	@CallSuper
	public void onDestroy() {

	}

	public View getView() {
		return view;
	}
}
