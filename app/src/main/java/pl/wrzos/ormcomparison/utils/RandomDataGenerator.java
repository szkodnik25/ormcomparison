package pl.wrzos.ormcomparison.utils;

import java.util.Random;

public class RandomDataGenerator {

	private static final Random RANDOM = new Random();

	public RandomDataGenerator() {
	}

	public boolean randomBool() {
		return RANDOM.nextBoolean();
	}

	public short randomShort() {
		return (short) RANDOM.nextInt(Short.MAX_VALUE + 1);
	}

	public int randomInt() {
		return RANDOM.nextInt();
	}

	public long randomLong() {
		return RANDOM.nextLong();
	}

	public float randomFloat() {
		return RANDOM.nextFloat();
	}

	public double randomDouble() {
		return RANDOM.nextDouble();
	}

	public String randomString(int length) {
		if (length < 0) {
			throw new IllegalArgumentException("Length of random string cannot be less than 0");
		}

		String characters = Constants.CHARACTERS;
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = characters.charAt(RANDOM.nextInt(characters.length()));
		}
		return new String(text);
	}

	public byte[] randomByteArray(int size) {
		if (size < 0) {
			throw new IllegalArgumentException("Size of random byte array cannot be less than 0");
		}

		byte[] bytes = new byte[size];
		RANDOM.nextBytes(bytes);
		return bytes;
	}

}
