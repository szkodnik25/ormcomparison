package pl.wrzos.ormcomparison.utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import pl.wrzos.ormcomparison.R;
import pl.wrzos.ormcomparison.chart.core.BenchmarkChartResult;

public class Preferences {

	public static final String BENCHMARK_RESULT = "benchmark_result";

	public static SharedPreferences getPreferences(Context context) {
		return context.getSharedPreferences(context.getString(R.string.preferences_key), Context.MODE_PRIVATE);
	}

	public static void saveBenchmarkResult(Context context, BenchmarkChartResult result) {
		getPreferences(context).edit().putString(BENCHMARK_RESULT, new Gson().toJson(result)).apply();
	}

	public static BenchmarkChartResult getBenchmarkResult(Context context) {
		String json = getPreferences(context).getString(BENCHMARK_RESULT, null);
		return json != null ? new Gson().fromJson(json, BenchmarkChartResult.class) : null;
	}
}
