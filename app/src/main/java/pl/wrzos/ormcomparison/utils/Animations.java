package pl.wrzos.ormcomparison.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Handler;
import android.view.View;
import android.view.ViewAnimationUtils;
import java.util.ArrayList;
import java.util.List;

public class Animations {

	public static void showViewWithCircularReveal(View view, AnimationFinishedListener listener) {
		if (!view.isAttachedToWindow()) {
			return;
		}
		int   cx          = view.getWidth() / 2;
		int   cy          = view.getHeight() / 2;
		float finalRadius = (float) Math.hypot(cx, cy);
		Animator anim =
			ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);
		view.setVisibility(View.VISIBLE);
		anim.addListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				super.onAnimationEnd(animation);
				if (listener != null) {
					listener.onAnimationFinished(animation);
				}
			}
		});
		anim.start();
	}

	public static void showViewWithCircularReveal(View view) {
		showViewWithCircularReveal(view, null);
	}

	public static void hideViewWithCircularEffect(View view) {
		if (!view.isAttachedToWindow()) {
			return;
		}
		int   cx            = view.getWidth() / 2;
		int   cy            = view.getHeight() / 2;
		float initialRadius = (float) Math.hypot(cx, cy);
		Animator anim =
			ViewAnimationUtils.createCircularReveal(view, cx, cy, initialRadius, 0);
		anim.addListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				super.onAnimationEnd(animation);
				view.setVisibility(View.INVISIBLE);
			}
		});
		anim.start();
	}

	public static void showAll(List<View> views, long delay) {
		Handler    handler = new Handler();
		List<Long> delays  = new ArrayList<>(views.size());
		delays.add(0L);
		for (int i = 1; i < views.size(); i++) {
			delays.add(delays.get(i - 1) + delay);
		}
		for (int i = 0; i < views.size(); i++) {
			final View toShow = views.get(i);
			handler.postDelayed(() -> showViewWithCircularReveal(toShow), delays.get(i));
		}
	}

	public static void hideAll(List<View> views, long delay) {
		Handler    handler = new Handler();
		List<Long> delays  = new ArrayList<>(views.size());
		delays.add(0L);
		for (int i = 1; i < views.size(); i++) {
			delays.add(delays.get(i - 1) + delay);
		}
		for (int i = 0; i < views.size(); i++) {
			final View toHide = views.get(i);
			handler.postDelayed(() -> hideViewWithCircularEffect(toHide), delays.get(i));
		}
	}

	public static void showView(View view) {
		view.setVisibility(View.VISIBLE);
		view.setAlpha(0);
		view.animate().alpha(1).start();
	}

	public static void hideView(View view) {
		view.animate().alpha(0).withEndAction(() -> view.setVisibility(View.INVISIBLE)).start();
	}

	public interface AnimationFinishedListener {
		void onAnimationFinished(Animator animator);
	}
}
