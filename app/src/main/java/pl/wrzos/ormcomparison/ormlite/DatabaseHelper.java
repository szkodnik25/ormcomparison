package pl.wrzos.ormcomparison.ormlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    public static final int DB_VERSION = 2;
    public static final String DB_NAME = "ormlite_db";

    private static DatabaseHelper instance;

    public static DatabaseHelper initialize(Context context, boolean isInMemory) {
        return instance = new DatabaseHelper(context, isInMemory);
    }

    private DatabaseHelper(Context context, boolean isInMemory) {
        super(context, (isInMemory ? null : DB_NAME), null, DB_VERSION);
    }

    public static DatabaseHelper getInstance() {
        if (instance == null) {
            throw new RuntimeException("You need to initialize first");
        }

        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }
}
