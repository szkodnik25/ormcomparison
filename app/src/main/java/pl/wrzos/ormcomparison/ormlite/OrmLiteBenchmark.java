package pl.wrzos.ormcomparison.ormlite;

import android.content.Context;
import android.util.Log;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;
import pl.wrzos.ormcomparison.benchmark.core.BenchmarkAttrsGenerator;
import pl.wrzos.ormcomparison.benchmark.core.BenchmarkConstants;
import pl.wrzos.ormcomparison.benchmark.core.BenchmarkExecutor;
import pl.wrzos.ormcomparison.benchmark.core.SupportedOrm;
import pl.wrzos.ormcomparison.utils.RandomDataGenerator;

public class OrmLiteBenchmark extends BenchmarkExecutor<OrmLiteEntity> {

	public static final String TAG = OrmLiteBenchmark.class.getSimpleName();

	private List<OrmLiteEntity> entities;
	private ConnectionSource    connectionSource;

	public OrmLiteBenchmark(Context context) {
		DatabaseHelper.initialize(context, false);
		connectionSource = DatabaseHelper.getInstance().getConnectionSource();
		entities = BenchmarkAttrsGenerator.getRandomOrmLiteList(BenchmarkConstants.NUM_OF_INSERTS);
	}

	@Override
	public void createDatabase() throws SQLException {
		TableUtils.createTable(connectionSource, OrmLiteEntity.class);
	}

	@Override
	public void writeData() throws SQLException {
		TransactionManager.callInTransaction(connectionSource, new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				for (OrmLiteEntity entity : entities) {
					DatabaseHelper.getInstance().getDao(OrmLiteEntity.class).createOrUpdate(entity);
				}
				return null;
			}
		});
	}

	@Override
	public List<OrmLiteEntity> readData() throws SQLException {
		List<OrmLiteEntity> entities = DatabaseHelper.getInstance().getDao(OrmLiteEntity.class).queryForAll();
		Log.d(TAG, "Read " + entities.size() + " rows");
		return entities;
	}

	@Override
	public void updateData(List<OrmLiteEntity> entitiesToUpdate) throws SQLException {
		TransactionManager.callInTransaction(connectionSource, new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				for (OrmLiteEntity entity : entitiesToUpdate) {
					DatabaseHelper.getInstance().getDao(OrmLiteEntity.class).update(entity);
				}
				return null;
			}
		});
	}

	@Override
	public void deleteData() throws SQLException {
		TableUtils.dropTable(DatabaseHelper.getInstance().getConnectionSource(), OrmLiteEntity.class, true);
	}

	@Override
	public SupportedOrm getOrm() {
		return SupportedOrm.ORM_LITE;
	}

	@Override
	protected void prepareDataForUpdate(List<OrmLiteEntity> dataToUpdate) {
		RandomDataGenerator generator = new RandomDataGenerator();
		for (OrmLiteEntity entity : dataToUpdate) {
			entity.setSimpleString(generator.randomString(BenchmarkConstants.STRING_LENGTH));
		}
	}
}
