package pl.wrzos.ormcomparison.ormlite;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Arrays;

@DatabaseTable(tableName = "ORM_LITE_ENTITY")
public class OrmLiteEntity {

    @DatabaseField(columnName = "_id", generatedId = true)
    private long id;

    @DatabaseField(columnName = "SIMPLE_BOOLEAN", dataType = DataType.BOOLEAN)
    private boolean simpleBoolean;
    @DatabaseField(columnName = "SIMPLE_SHORT", dataType = DataType.SHORT)
    private short simpleShort;
    @DatabaseField(columnName = "SIMPLE_INT", dataType = DataType.INTEGER)
    private int simpleInt;
    @DatabaseField(columnName = "SIMPLE_LONG", dataType = DataType.LONG)
    private long simpleLong;
    @DatabaseField(columnName = "SIMPLE_FLOAT", dataType = DataType.FLOAT)
    private float simpleFloat;
    @DatabaseField(columnName = "SIMPLE_DOUBLE", dataType = DataType.DOUBLE)
    private double simpleDouble;
    @DatabaseField(columnName = "SIMPLE_STRING", dataType = DataType.STRING)
    private String simpleString;
    @DatabaseField(dataType = DataType.BYTE_ARRAY, columnName = "SIMPLE_BYTE_ARRAY")
    private byte[] simpleByteArray;

    public OrmLiteEntity() {
    }

    public OrmLiteEntity(long id, boolean simpleBoolean, short simpleShort, int simpleInt, long simpleLong, float simpleFloat, double simpleDouble, String simpleString, byte[] simpleByteArray) {
        this.id = id;
        this.simpleBoolean = simpleBoolean;
        this.simpleShort = simpleShort;
        this.simpleInt = simpleInt;
        this.simpleLong = simpleLong;
        this.simpleFloat = simpleFloat;
        this.simpleDouble = simpleDouble;
        this.simpleString = simpleString;
        this.simpleByteArray = simpleByteArray;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isSimpleBoolean() {
        return simpleBoolean;
    }

    public void setSimpleBoolean(boolean simpleBoolean) {
        this.simpleBoolean = simpleBoolean;
    }

    public short getSimpleShort() {
        return simpleShort;
    }

    public void setSimpleShort(short simpleShort) {
        this.simpleShort = simpleShort;
    }

    public int getSimpleInt() {
        return simpleInt;
    }

    public void setSimpleInt(int simpleInt) {
        this.simpleInt = simpleInt;
    }

    public long getSimpleLong() {
        return simpleLong;
    }

    public void setSimpleLong(long simpleLong) {
        this.simpleLong = simpleLong;
    }

    public float getSimpleFloat() {
        return simpleFloat;
    }

    public void setSimpleFloat(float simpleFloat) {
        this.simpleFloat = simpleFloat;
    }

    public double getSimpleDouble() {
        return simpleDouble;
    }

    public void setSimpleDouble(double simpleDouble) {
        this.simpleDouble = simpleDouble;
    }

    public String getSimpleString() {
        return simpleString;
    }

    public void setSimpleString(String simpleString) {
        this.simpleString = simpleString;
    }

    public byte[] getSimpleByteArray() {
        return simpleByteArray;
    }

    public void setSimpleByteArray(byte[] simpleByteArray) {
        this.simpleByteArray = simpleByteArray;
    }

    @Override
    public String toString() {
        return "OrmLiteEntity{" +
                "id=" + id +
                ", simpleBoolean=" + simpleBoolean +
                ", simpleShort=" + simpleShort +
                ", simpleInt=" + simpleInt +
                ", simpleLong=" + simpleLong +
                ", simpleFloat=" + simpleFloat +
                ", simpleDouble=" + simpleDouble +
                ", simpleString='" + simpleString + '\'' +
                ", simpleByteArray=" + Arrays.toString(simpleByteArray) +
                '}';
    }
}
