package pl.wrzos.ormcomparison.sugarorm;

import com.orm.SugarRecord;

import java.util.Arrays;

public class SugarOrmEntity extends SugarRecord<SugarOrmEntity> {

    private Long id;
    private Boolean simpleBoolean;
    private Short simpleShort;
    private Integer simpleInt;
    private Long simpleLong;
    private Float simpleFloat;
    private Double simpleDouble;
    private String simpleString;
    private byte[] simpleByteArray;

    public SugarOrmEntity() {
    }

    public SugarOrmEntity(Long id, Boolean simpleBoolean, Short simpleShort, Integer simpleInt, Long simpleLong, Float simpleFloat, Double simpleDouble, String simpleString, byte[] simpleByteArray) {
        this.id = id;
        this.simpleBoolean = simpleBoolean;
        this.simpleShort = simpleShort;
        this.simpleInt = simpleInt;
        this.simpleLong = simpleLong;
        this.simpleFloat = simpleFloat;
        this.simpleDouble = simpleDouble;
        this.simpleString = simpleString;
        this.simpleByteArray = simpleByteArray;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getSimpleBoolean() {
        return simpleBoolean;
    }

    public void setSimpleBoolean(Boolean simpleBoolean) {
        this.simpleBoolean = simpleBoolean;
    }

    public Short getSimpleShort() {
        return simpleShort;
    }

    public void setSimpleShort(Short simpleShort) {
        this.simpleShort = simpleShort;
    }

    public Integer getSimpleInt() {
        return simpleInt;
    }

    public void setSimpleInt(Integer simpleInt) {
        this.simpleInt = simpleInt;
    }

    public Long getSimpleLong() {
        return simpleLong;
    }

    public void setSimpleLong(Long simpleLong) {
        this.simpleLong = simpleLong;
    }

    public Float getSimpleFloat() {
        return simpleFloat;
    }

    public void setSimpleFloat(Float simpleFloat) {
        this.simpleFloat = simpleFloat;
    }

    public Double getSimpleDouble() {
        return simpleDouble;
    }

    public void setSimpleDouble(Double simpleDouble) {
        this.simpleDouble = simpleDouble;
    }

    public String getSimpleString() {
        return simpleString;
    }

    public void setSimpleString(String simpleString) {
        this.simpleString = simpleString;
    }

    public byte[] getSimpleByteArray() {
        return simpleByteArray;
    }

    public void setSimpleByteArray(byte[] simpleByteArray) {
        this.simpleByteArray = simpleByteArray;
    }

    @Override
    public String toString() {
        return "SugarOrmEntity{" +
                "id=" + id +
                ", simpleBoolean=" + simpleBoolean +
                ", simpleShort=" + simpleShort +
                ", simpleInt=" + simpleInt +
                ", simpleLong=" + simpleLong +
                ", simpleFloat=" + simpleFloat +
                ", simpleDouble=" + simpleDouble +
                ", simpleString='" + simpleString + '\'' +
                ", simpleByteArray=" + Arrays.toString(simpleByteArray) +
                '}';
    }
}
