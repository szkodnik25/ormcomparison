package pl.wrzos.ormcomparison.sugarorm;

import android.util.Log;
import com.orm.SugarRecord;
import com.orm.SugarTransactionHelper;
import java.sql.SQLException;
import java.util.List;
import pl.wrzos.ormcomparison.benchmark.core.BenchmarkAttrsGenerator;
import pl.wrzos.ormcomparison.benchmark.core.BenchmarkConstants;
import pl.wrzos.ormcomparison.benchmark.core.BenchmarkExecutor;
import pl.wrzos.ormcomparison.benchmark.core.SupportedOrm;
import pl.wrzos.ormcomparison.utils.RandomDataGenerator;

public class SugarOrmBenchmark extends BenchmarkExecutor<SugarOrmEntity> {

	public static final String TAG = SugarOrmBenchmark.class.getSimpleName();

	private List<SugarOrmEntity> entities;

	public SugarOrmBenchmark() {
		entities = BenchmarkAttrsGenerator.getRandomSugarOrmList(BenchmarkConstants.NUM_OF_INSERTS);
	}

	@Override
	public void createDatabase() {

	}

	@Override
	public void writeData() {
		SugarTransactionHelper.doInTansaction(() -> entities.forEach(SugarRecord::save));
	}

	@Override
	public List<SugarOrmEntity> readData() {
		List<SugarOrmEntity> entities = SugarOrmEntity.listAll(SugarOrmEntity.class);
		Log.d(TAG, "Read " + entities.size() + " rows");
		return entities;
	}

	@Override
	public void updateData(List<SugarOrmEntity> entitiesToUpdate) throws SQLException {
		SugarTransactionHelper.doInTansaction(() -> entitiesToUpdate.forEach(SugarRecord::save));
	}

	@Override
	public void deleteData() {
		SugarOrmEntity.deleteAll(SugarOrmEntity.class);
	}

	@Override
	public SupportedOrm getOrm() {
		return SupportedOrm.SUGAR_ORM;
	}

	@Override
	protected void prepareDataForUpdate(List<SugarOrmEntity> dataToUpdate) {
		RandomDataGenerator generator = new RandomDataGenerator();
		for (SugarOrmEntity entity : dataToUpdate) {
			entity.setSimpleString(generator.randomString(BenchmarkConstants.STRING_LENGTH));
		}
	}
}
