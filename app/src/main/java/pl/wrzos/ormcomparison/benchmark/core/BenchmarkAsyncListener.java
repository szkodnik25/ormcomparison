package pl.wrzos.ormcomparison.benchmark.core;

public interface BenchmarkAsyncListener {
	void onPreExecute();
	void onResult(BenchmarkResult result);
}
