package pl.wrzos.ormcomparison.benchmark.core;

import android.os.Parcel;

public class BenchmarkResult implements android.os.Parcelable {

	private SupportedOrm orm;
	private long         createDataResult;
	private long         writeDataResult;
	private long         readDataResult;
	private long         updateDataResult;
	private long         deleteDataResult;

	public BenchmarkResult() {
		createDataResult = 0L;
		writeDataResult = 0L;
		readDataResult = 0L;
		updateDataResult = 0L;
		deleteDataResult = 0L;
	}

	public BenchmarkResult(
		SupportedOrm orm, long createDataResult, long writeDataResult, long readDataResult, long updateDataResult,
		long deleteDataResult) {
		this.orm = orm;
		this.createDataResult = createDataResult;
		this.writeDataResult = writeDataResult;
		this.readDataResult = readDataResult;
		this.updateDataResult = updateDataResult;
		this.deleteDataResult = deleteDataResult;
	}

	public SupportedOrm getOrm() {
		return orm;
	}

	public void setOrm(SupportedOrm orm) {
		this.orm = orm;
	}

	public long getCreateDataResult() {
		return createDataResult;
	}

	public void setCreateDataResult(long createDataResult) {
		this.createDataResult = createDataResult;
	}

	public long getWriteDataResult() {
		return writeDataResult;
	}

	public void setWriteDataResult(long writeDataResult) {
		this.writeDataResult = writeDataResult;
	}

	public long getReadDataResult() {
		return readDataResult;
	}

	public void setReadDataResult(long readDataResult) {
		this.readDataResult = readDataResult;
	}

	public long getUpdateDataResult() {
		return updateDataResult;
	}

	public void setUpdateDataResult(long updateDataResult) {
		this.updateDataResult = updateDataResult;
	}

	public long getDeleteDataResult() {
		return deleteDataResult;
	}

	public void setDeleteDataResult(long deleteDataResult) {
		this.deleteDataResult = deleteDataResult;
	}

	public double getSummaryInSec() {
		long sum = createDataResult + writeDataResult + readDataResult + updateDataResult + deleteDataResult;
		return ((double) sum / 1000);
	}

	@Override
	public String toString() {
		return "BenchmarkResult{" +
			"orm=" + orm +
			", createDataResult=" + createDataResult +
			", writeDataResult=" + writeDataResult +
			", readDataResult=" + readDataResult +
			", updateDataResult=" + updateDataResult +
			", deleteDataResult=" + deleteDataResult +
			'}';
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.orm == null ? -1 : this.orm.ordinal());
		dest.writeLong(this.createDataResult);
		dest.writeLong(this.writeDataResult);
		dest.writeLong(this.readDataResult);
		dest.writeLong(this.updateDataResult);
		dest.writeLong(this.deleteDataResult);
	}

	protected BenchmarkResult(Parcel in) {
		int tmpOrm = in.readInt();
		this.orm = tmpOrm == -1 ? null : SupportedOrm.values()[tmpOrm];
		this.createDataResult = in.readLong();
		this.writeDataResult = in.readLong();
		this.readDataResult = in.readLong();
		this.updateDataResult = in.readLong();
		this.deleteDataResult = in.readLong();
	}

	public static final Creator<BenchmarkResult> CREATOR = new Creator<BenchmarkResult>() {
		@Override
		public BenchmarkResult createFromParcel(Parcel source) {
			return new BenchmarkResult(source);
		}

		@Override
		public BenchmarkResult[] newArray(int size) {
			return new BenchmarkResult[size];
		}
	};
}
