package pl.wrzos.ormcomparison.benchmark.core;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class BenchmarkExecutor<T> implements BenchmarkExecutable<T> {

	protected List<T> loadedData = new ArrayList<>();

	public long executeCreateDatabase() throws SQLException {
		long start = System.currentTimeMillis();
		createDatabase();
		return System.currentTimeMillis() - start;
	}

	public long executeWriteData() throws SQLException {
		long start = System.currentTimeMillis();
		writeData();
		return System.currentTimeMillis() - start;
	}

	public long executeReadData() throws SQLException {
		long start = System.currentTimeMillis();
		loadedData = readData();
		return System.currentTimeMillis() - start;
	}

	public long executeUpdateData() throws SQLException {
		prepareDataForUpdate(loadedData);
		long start = System.currentTimeMillis();
		updateData(loadedData);
		return System.currentTimeMillis() - start;
	}

	public long executeDeleteData() throws SQLException {
		long start = System.currentTimeMillis();
		deleteData();
		return System.currentTimeMillis() - start;
	}

	public abstract SupportedOrm getOrm();

	protected abstract void prepareDataForUpdate(List<T> dataToUpdate);

	public BenchmarkResult performSync() {
		return BenchmarkTask.runSync(this);
	}

	public void performAsync(BenchmarkAsyncListener listener) {
		BenchmarkTask.runAsync(this, listener);
	}
}
