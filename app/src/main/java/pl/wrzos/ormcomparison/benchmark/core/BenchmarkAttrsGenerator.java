package pl.wrzos.ormcomparison.benchmark.core;

import java.util.ArrayList;
import java.util.List;
import pl.wrzos.ormcomparison.greendao.GreenDaoEntity;
import pl.wrzos.ormcomparison.ormlite.OrmLiteEntity;
import pl.wrzos.ormcomparison.sugarorm.SugarOrmEntity;
import pl.wrzos.ormcomparison.utils.RandomDataGenerator;

public class BenchmarkAttrsGenerator {

	public static List<GreenDaoEntity> getRandomGreenDaoList(int size) {
		RandomDataGenerator generator = new RandomDataGenerator();
		List<GreenDaoEntity> entities = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			GreenDaoEntity entity = new GreenDaoEntity();
			entity.setSimple_boolean(generator.randomBool());
			entity.setSimple_short(generator.randomShort());
			entity.setSimple_int(generator.randomInt());
			entity.setSimple_long(generator.randomLong());
			entity.setSimple_float(generator.randomFloat());
			entity.setSimple_double(generator.randomDouble());
			entity.setSimple_string(generator.randomString(BenchmarkConstants.STRING_LENGTH));
			entity.setSimple_byte_array(generator.randomByteArray(BenchmarkConstants.BYTE_ARRAY_SIZE));
			entities.add(entity);
		}
		return entities;
	}

	public static List<OrmLiteEntity> getRandomOrmLiteList(int size) {
		RandomDataGenerator generator = new RandomDataGenerator();
		List<OrmLiteEntity> entities = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			OrmLiteEntity entity = new OrmLiteEntity();
			entity.setSimpleBoolean(generator.randomBool());
			entity.setSimpleShort(generator.randomShort());
			entity.setSimpleInt(generator.randomInt());
			entity.setSimpleLong(generator.randomLong());
			entity.setSimpleFloat(generator.randomFloat());
			entity.setSimpleDouble(generator.randomDouble());
			entity.setSimpleString(generator.randomString(BenchmarkConstants.STRING_LENGTH));
			entity.setSimpleByteArray(generator.randomByteArray(BenchmarkConstants.BYTE_ARRAY_SIZE));
			entities.add(entity);
		}
		return entities;
	}

	public static List<SugarOrmEntity> getRandomSugarOrmList(int size) {
		RandomDataGenerator generator = new RandomDataGenerator();
		List<SugarOrmEntity> entities = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			SugarOrmEntity entity = new SugarOrmEntity();
			entity.setSimpleBoolean(generator.randomBool());
			entity.setSimpleShort(generator.randomShort());
			entity.setSimpleInt(generator.randomInt());
			entity.setSimpleLong(generator.randomLong());
			entity.setSimpleFloat(generator.randomFloat());
			entity.setSimpleDouble(generator.randomDouble());
			entity.setSimpleString(generator.randomString(BenchmarkConstants.STRING_LENGTH));
			entity.setSimpleByteArray(generator.randomByteArray(BenchmarkConstants.BYTE_ARRAY_SIZE));
			entities.add(entity);
		}
		return entities;
	}

}
