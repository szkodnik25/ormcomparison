package pl.wrzos.ormcomparison.benchmark.core;

import java.sql.SQLException;
import java.util.List;

interface BenchmarkExecutable<T> {

	void createDatabase() throws SQLException;

	void writeData() throws SQLException;

	List<T> readData() throws SQLException;

	void updateData(List<T> entitiesToUpdate) throws SQLException;

	void deleteData() throws SQLException;

}
