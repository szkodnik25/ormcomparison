package pl.wrzos.ormcomparison.benchmark.core;

import android.os.AsyncTask;
import java.sql.SQLException;

public class BenchmarkTask {

	public static BenchmarkResult runSync(BenchmarkExecutor benchmarkExecutor) {
		BenchmarkResult benchmarkResult = new BenchmarkResult();
		try {
			benchmarkResult.setOrm(benchmarkExecutor.getOrm());
			benchmarkResult.setCreateDataResult(benchmarkExecutor.executeCreateDatabase());
			benchmarkResult.setWriteDataResult(benchmarkExecutor.executeWriteData());
			benchmarkResult.setReadDataResult(benchmarkExecutor.executeReadData());
			benchmarkResult.setUpdateDataResult(benchmarkExecutor.executeUpdateData());
			benchmarkResult.setDeleteDataResult(benchmarkExecutor.executeDeleteData());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return benchmarkResult;
	}

	public static void runAsync(BenchmarkExecutor benchmarkExecutor, BenchmarkAsyncListener listener) {
		new BenchmarkAsyncTask(benchmarkExecutor).withListener(listener);
	}

	private static class BenchmarkAsyncTask extends AsyncTask<Void, Void, BenchmarkResult> {

		private BenchmarkExecutor      benchmarkExecutor;
		private BenchmarkAsyncListener benchmarkAsyncListener;

		public BenchmarkAsyncTask(BenchmarkExecutor benchmarkExecutor) {
			this.benchmarkExecutor = benchmarkExecutor;
		}

		public BenchmarkAsyncTask withListener(BenchmarkAsyncListener benchmarkAsyncListener) {
			this.benchmarkAsyncListener = benchmarkAsyncListener;
			return this;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (benchmarkAsyncListener != null) {
				benchmarkAsyncListener.onPreExecute();
			}
		}

		@Override
		protected BenchmarkResult doInBackground(Void... voids) {
			return runSync(benchmarkExecutor);
		}

		@Override
		protected void onPostExecute(BenchmarkResult benchmarkResult) {
			super.onPostExecute(benchmarkResult);
			if (benchmarkAsyncListener != null) {
				benchmarkAsyncListener.onResult(benchmarkResult);
			}
		}
	}
}
