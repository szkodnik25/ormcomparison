package pl.wrzos.ormcomparison.benchmark.core;

public class BenchmarkConstants {
	public static final int NUM_OF_INSERTS = 10000;
	public static final int STRING_LENGTH = 100;
	public static final int BYTE_ARRAY_SIZE = 100;
}
