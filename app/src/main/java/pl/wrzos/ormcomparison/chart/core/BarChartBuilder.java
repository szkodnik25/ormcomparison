package pl.wrzos.ormcomparison.chart.core;

import com.github.mikephil.charting.charts.BarChart;

public class BarChartBuilder implements ChartBuilder<BarChart, SingleOperationResult> {

	@Override
	public Displayable<SingleOperationResult> on(BarChart chart) {
		return new BarChartDispatcher(chart);
	}

}
