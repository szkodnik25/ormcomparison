package pl.wrzos.ormcomparison.chart.core;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import pl.wrzos.ormcomparison.benchmark.core.BenchmarkResult;
import pl.wrzos.ormcomparison.benchmark.core.SupportedOrm;
import pl.wrzos.ormcomparison.chart.ChartUtils;

public class BenchmarkChartResult implements Serializable {

	private Date benchmarkDate;

	private SingleOperationResult createResult;
	private SingleOperationResult writeResult;
	private SingleOperationResult readResult;
	private SingleOperationResult updateResult;
	private SingleOperationResult deleteResult;

	public BenchmarkChartResult() {
	}

	public static BenchmarkChartResult wrap(List<BenchmarkResult> results) {
		BenchmarkChartResult wrapper  = new BenchmarkChartResult();
		BenchmarkResult      greenDao = ChartUtils.getOrmFromList(SupportedOrm.GREEN_DAO, results);
		BenchmarkResult      ormLite  = ChartUtils.getOrmFromList(SupportedOrm.ORM_LITE, results);
		BenchmarkResult      sugarOrm = ChartUtils.getOrmFromList(SupportedOrm.SUGAR_ORM, results);
		wrapper.setCreateResult(SingleOperationResult.instantiate(
			greenDao.getCreateDataResult(), ormLite.getCreateDataResult(), sugarOrm.getCreateDataResult()));
		wrapper.setWriteResult(SingleOperationResult.instantiate(
			greenDao.getWriteDataResult(), ormLite.getWriteDataResult(), sugarOrm.getWriteDataResult()));
		wrapper.setReadResult(SingleOperationResult.instantiate(
			greenDao.getReadDataResult(), ormLite.getReadDataResult(), sugarOrm.getReadDataResult()));
		wrapper.setUpdateResult(SingleOperationResult.instantiate(
			greenDao.getUpdateDataResult(), ormLite.getUpdateDataResult(), sugarOrm.getUpdateDataResult()));
		wrapper.setDeleteResult(SingleOperationResult.instantiate(
			greenDao.getDeleteDataResult(), ormLite.getDeleteDataResult(), sugarOrm.getDeleteDataResult()));
		return wrapper;
	}

	public Date getBenchmarkDate() {
		return benchmarkDate;
	}

	public void setBenchmarkDate(Date benchmarkDate) {
		this.benchmarkDate = benchmarkDate;
	}

	public SingleOperationResult getCreateResult() {
		return createResult;
	}

	public void setCreateResult(SingleOperationResult createResult) {
		this.createResult = createResult;
	}

	public SingleOperationResult getWriteResult() {
		return writeResult;
	}

	public void setWriteResult(SingleOperationResult writeResult) {
		this.writeResult = writeResult;
	}

	public SingleOperationResult getReadResult() {
		return readResult;
	}

	public void setReadResult(SingleOperationResult readResult) {
		this.readResult = readResult;
	}

	public SingleOperationResult getUpdateResult() {
		return updateResult;
	}

	public void setUpdateResult(SingleOperationResult updateResult) {
		this.updateResult = updateResult;
	}

	public SingleOperationResult getDeleteResult() {
		return deleteResult;
	}

	public void setDeleteResult(SingleOperationResult deleteResult) {
		this.deleteResult = deleteResult;
	}
}
