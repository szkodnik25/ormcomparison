package pl.wrzos.ormcomparison.chart.core;

import com.github.mikephil.charting.charts.Chart;

public interface ChartBuilder<ToBuild extends Chart, Data> {
	Displayable<Data> on(ToBuild chart);
}
