package pl.wrzos.ormcomparison.chart.core;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import pl.wrzos.ormcomparison.benchmark.core.SupportedOrm;

public class SingleOperationResult implements Serializable {

	private HashMap<SupportedOrm, Long> result;

	public SingleOperationResult() {
		result = new HashMap<>();
	}

	public Map<SupportedOrm, Long> getResult() {
		return result;
	}

	public void setResult(HashMap<SupportedOrm, Long> result) {
		this.result = result;
	}

	public static SingleOperationResult instantiate(Long greenDao, Long ormLite, Long sugarOrm) {
		SingleOperationResult       singleOperationResult = new SingleOperationResult();
		HashMap<SupportedOrm, Long> resultMap             = new HashMap<>();
		resultMap.put(SupportedOrm.GREEN_DAO, greenDao);
		resultMap.put(SupportedOrm.ORM_LITE, ormLite);
		resultMap.put(SupportedOrm.SUGAR_ORM, sugarOrm);
		singleOperationResult.setResult(resultMap);
		return singleOperationResult;
	}
}
