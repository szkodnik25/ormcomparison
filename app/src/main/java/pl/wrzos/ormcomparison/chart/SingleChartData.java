package pl.wrzos.ormcomparison.chart;

public class SingleChartData {

	private float greenDaoResult;
	private float ormLiteResult;
	private float sugarOrmResult;

	public SingleChartData() {
	}

	public SingleChartData(float greenDaoResult, float ormLiteResult, float sugarOrmResult) {
		this.greenDaoResult = greenDaoResult;
		this.ormLiteResult = ormLiteResult;
		this.sugarOrmResult = sugarOrmResult;
	}

	public float getGreenDaoResult() {
		return greenDaoResult;
	}

	public void setGreenDaoResult(float greenDaoResult) {
		this.greenDaoResult = greenDaoResult;
	}

	public float getOrmLiteResult() {
		return ormLiteResult;
	}

	public void setOrmLiteResult(float ormLiteResult) {
		this.ormLiteResult = ormLiteResult;
	}

	public float getSugarOrmResult() {
		return sugarOrmResult;
	}

	public void setSugarOrmResult(float sugarOrmResult) {
		this.sugarOrmResult = sugarOrmResult;
	}
}
