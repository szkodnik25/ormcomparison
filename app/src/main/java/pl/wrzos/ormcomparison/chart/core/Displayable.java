package pl.wrzos.ormcomparison.chart.core;

public interface Displayable<T> {
	void show(T data);
}
