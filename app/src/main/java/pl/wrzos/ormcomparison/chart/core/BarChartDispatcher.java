package pl.wrzos.ormcomparison.chart.core;

import com.github.mikephil.charting.animation.Easing.EasingOption;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import java.util.ArrayList;
import java.util.List;
import pl.wrzos.ormcomparison.R;
import pl.wrzos.ormcomparison.benchmark.core.SupportedOrm;

public class BarChartDispatcher implements Displayable<SingleOperationResult> {

	private BarChart barChart;

	BarChartDispatcher(BarChart barChart) {
		this.barChart = barChart;
	}

	@Override
	public void show(SingleOperationResult data) {
		List<BarEntry> entries = new ArrayList<>();
		entries.add(new BarEntry(0f, data.getResult().get(SupportedOrm.GREEN_DAO)));
		entries.add(new BarEntry(1f, data.getResult().get(SupportedOrm.ORM_LITE)));
		entries.add(new BarEntry(2f, data.getResult().get(SupportedOrm.SUGAR_ORM)));

		BarDataSet set = new BarDataSet(entries, "BarDataSet");
		set.setColors(
			new int[]{R.color.green_dao_color, R.color.orm_lite_color, R.color.sugar_orm_color}, barChart.getContext());
		BarData barData = new BarData(set);
		barChart.setData(barData);
		barChart.setDescription(null);
		barChart.getLegend().setEnabled(false);
		barChart.setFitBars(true);

		barChart.setTouchEnabled(false);
		barChart.setDrawGridBackground(false);
		barChart.getAxisLeft().setDrawGridLines(false);
		barChart.getAxisRight().setDrawLabels(false);

		barChart.getXAxis().setEnabled(false);

		barChart.getXAxis().setDrawGridLines(false);

		barChart.animateY(2000, EasingOption.EaseOutBounce);
	}
}
