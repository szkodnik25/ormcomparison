package pl.wrzos.ormcomparison.chart;

import java.util.List;
import pl.wrzos.ormcomparison.benchmark.core.BenchmarkResult;
import pl.wrzos.ormcomparison.benchmark.core.SupportedOrm;

public class ChartUtils {

	public static BenchmarkResult getOrmFromList(SupportedOrm orm, List<BenchmarkResult> benchmarkResults) {
		for (BenchmarkResult result : benchmarkResults) {
			if (result.getOrm().equals(orm)) {
				return result;
			}
		}
		throw new IllegalArgumentException("Didn't found supported ORM");
	}

}
