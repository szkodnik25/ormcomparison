package pl.wrzos.ormcomparison.greendao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.sql.SQLException;
import java.util.List;
import org.greenrobot.greendao.database.StandardDatabase;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import pl.wrzos.ormcomparison.benchmark.core.BenchmarkAttrsGenerator;
import pl.wrzos.ormcomparison.benchmark.core.BenchmarkConstants;
import pl.wrzos.ormcomparison.benchmark.core.BenchmarkExecutor;
import pl.wrzos.ormcomparison.benchmark.core.SupportedOrm;
import pl.wrzos.ormcomparison.utils.RandomDataGenerator;

public class GreenDaoBenchmark extends BenchmarkExecutor<GreenDaoEntity> {

	public static final String TAG           = GreenDaoBenchmark.class.getSimpleName();
	public static final String DATABASE_NAME = "greendao_db";

	private GreenDaoHelper       greenDaoHelper;
	private DaoSession           daoSession;
	private List<GreenDaoEntity> greenDaoEntities;

	public GreenDaoBenchmark(Context context) {
		greenDaoEntities = BenchmarkAttrsGenerator.getRandomGreenDaoList(BenchmarkConstants.NUM_OF_INSERTS);
		greenDaoHelper = new GreenDaoHelper(context, DATABASE_NAME, null);
	}

	@Override
	public void createDatabase() {
		final StandardDatabase db = new StandardDatabase(greenDaoHelper.getWritableDatabase());
		if (daoSession == null) {
			daoSession = new DaoMaster(db).newSession(IdentityScopeType.None);
		} else {
			DaoMaster.createAllTables(db, true);
		}
	}

	@Override
	public void writeData() {
		daoSession.getGreenDaoEntityDao().insertInTx(greenDaoEntities);
		daoSession.clear();
	}

	@Override
	public List<GreenDaoEntity> readData() {
		List<GreenDaoEntity> entities = daoSession.getGreenDaoEntityDao().loadAll();
		Log.d(TAG, "Read " + entities.size() + " rows");
		return entities;
	}

	@Override
	public void updateData(List<GreenDaoEntity> entitiesToUpdate) throws SQLException {
		daoSession.getGreenDaoEntityDao().updateInTx(entitiesToUpdate);
	}

	@Override
	public void deleteData() {
		final SQLiteDatabase sqLiteDatabase = greenDaoHelper.getWritableDatabase();
		final StandardDatabase db = new StandardDatabase(sqLiteDatabase);
		DaoMaster.dropAllTables(db, true);
		sqLiteDatabase.setVersion(0);
	}

	@Override
	public SupportedOrm getOrm() {
		return SupportedOrm.GREEN_DAO;
	}

	@Override
	protected void prepareDataForUpdate(List<GreenDaoEntity> dataToUpdate) {
		RandomDataGenerator generator = new RandomDataGenerator();
		for (GreenDaoEntity entity : dataToUpdate) {
			entity.setSimple_string(generator.randomString(BenchmarkConstants.STRING_LENGTH));
		}
	}
}
