package pl.wrzos.ormcomparison.greendao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import org.greenrobot.greendao.database.Database;
import pl.wrzos.ormcomparison.greendao.DaoMaster.DevOpenHelper;

public class GreenDaoHelper extends DevOpenHelper {

	public GreenDaoHelper(Context context, String name,
		CursorFactory factory) {
		super(context, name, factory);
	}

	@Override
	public void onCreate(Database db) {
		super.onCreate(db);
	}

	@Override
	public void onUpgrade(Database db, int oldVersion, int newVersion) {
		super.onUpgrade(db, oldVersion, newVersion);
	}

	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		super.onDowngrade(db, oldVersion, newVersion);
	}
}
