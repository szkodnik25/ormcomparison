package pl.wrzos.ormcomparison;

import com.facebook.stetho.Stetho;
import com.orm.Database;
import com.orm.SugarApp;

public class OrmComparisonApp extends SugarApp {

    private static OrmComparisonApp instance;

    public static OrmComparisonApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Stetho.initializeWithDefaults(this);
    }

    public Database getSugarDatabase() {
        return getDatabase();
    }
}
