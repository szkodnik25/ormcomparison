package pl.wrzos.ormcomparison.generator;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class Generator {

	public static final int DB_VERSION = 2;

	public static final String PACKAGE = "pl.wrzos.ormcomparison.greendao";
	public static final String PATH = "../app/src/main/java";

	public static final String ENTITY_NAME = "GreenDaoEntity";

	public static final String SIMPLE_BOOLEAN    = "simple_boolean";
	public static final String SIMPLE_SHORT      = "simple_short";
	public static final String SIMPLE_INT        = "simple_int";
	public static final String SIMPLE_LONG       = "simple_long";
	public static final String SIMPLE_FLOAT      = "simple_float";
	public static final String SIMPLE_DOUBLE     = "simple_double";
	public static final String SIMPLE_STRING     = "simple_string";
	public static final String SIMPLE_BYTE_ARRAY = "simple_byte_array";

	public static void main(String[] args) {
		Schema schema = new Schema(DB_VERSION, PACKAGE);

		Entity simpleEntity = schema.addEntity(ENTITY_NAME);
		simpleEntity.addIdProperty().autoincrement();
		simpleEntity.addBooleanProperty(SIMPLE_BOOLEAN);
		simpleEntity.addShortProperty(SIMPLE_SHORT);
		simpleEntity.addIntProperty(SIMPLE_INT);
		simpleEntity.addLongProperty(SIMPLE_LONG);
		simpleEntity.addFloatProperty(SIMPLE_FLOAT);
		simpleEntity.addDoubleProperty(SIMPLE_DOUBLE);
		simpleEntity.addStringProperty(SIMPLE_STRING);
		simpleEntity.addByteArrayProperty(SIMPLE_BYTE_ARRAY);

		try {
			new DaoGenerator().generateAll(schema, PATH);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
